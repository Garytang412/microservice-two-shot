from django.db import models

# Create your models here.
class BinVO(models.Model):
    import_href = models.CharField(max_length=200, unique=True)
    name = models.CharField(max_length=100)


class Shoe(models.Model):
    #unique id included
    manufacturer = models.CharField(max_length=60)
    model_name = models.CharField(max_length=100)
    color = models.CharField(max_length=50)
    pic_url = models.URLField()


    bin = models.ForeignKey(BinVO, related_name='shoes', on_delete=models.CASCADE)

    def __str__(self) -> str:
        return f"{self.manufacturer}, {self.model_name}, {self.color} Shoe"