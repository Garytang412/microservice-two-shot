from pyexpat import model
from common.json import ModelEncoder
from hats_rest.models import Hat, LocationVO

class LocationVODetailEncoder(ModelEncoder):
    model = LocationVO
    properties = ["name","import_href","id"]

class HatListEncoder(ModelEncoder):
    model = Hat
    properties = ['fabric','style_name',"id"]

class HatDetailEncoder(ModelEncoder):
    model = Hat
    properties = ['fabric','style_name',"color","pic_url","id","location"]
    encoders = {
        "location": LocationVODetailEncoder(),
    }

