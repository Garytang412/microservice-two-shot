import React, { Component } from 'react';
import shoeDetail from './shoeDetail';
import Accordion from './Accordion';
import { Link } from 'react-router-dom';


// export default class Shoes extends Component {

//     render() {
//         return (
            <div className='grid text-center mt-5'>
                <div className="container row">
                    <h1 className='col-10' >Here are the shoes:</h1>
                    <button className='col-2 btn btn-success'>Add a Shoe</button>
                </div>
            </div>
//         )
//     }
// }


export default class Shoes extends Component {
    constructor(props) {
        super(props);
        this.state = {
            shoes: [],
        };
    }

    async componentDidMount(){
        const request = await fetch("http://localhost:8080/api/shoes/");
        const shoeData = await request.json();
        this.setState({
            shoes: shoeData,
        })
        console.log(this.state.shoes);
    }

    render() {
        console.log(this.state.shoes)
        return (
            
            <div className='grid text-center'>
            <h1>Displaying Shoes:</h1>
            <Link to="/shoes/add" className='col-2 btn btn-success'>Add shoe</Link>
            {/* {this.state.shoes.map((shoe)=><Shoe data={shoe}/>)} */}
            <Accordion />
            </div>
        )
    }
}

<div className='mb-5 bg-warning'><shoeDetail /></div>



