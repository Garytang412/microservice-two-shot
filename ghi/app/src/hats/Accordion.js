import AccordionItem from './AccordionItem';
import { useState, useEffect } from 'react';

import React from 'react'

export default function Accordion() {
    const [hats, setHats] = useState([]);
    const [refresh, setRefresh] = useState(1);

    useEffect(() => {
        console.log("refreshed!");
        fetch("http://localhost:8090/api/hats/")
            .then(res => res.json())
            .then(res => res)
            .then(data => setHats(data.hats))
    }, [refresh])


    return (
        <div id="accordion">
            {hats.map(hat => <AccordionItem key={hat.id} data={hat} refresh={refresh} setRefresh={setRefresh} />)}
        </div>
    )
}


            // export default class Accordion extends Component {
            //     constructor(props) {
            //         super(props);
            //         this.state = {
            //             hats: [],
            //             refresh: 1
            //         }
            //     }
            //     refresh = () => {
            //         this.setState({
            //             refresh: this.state.refresh + 1
            //         })
            //         console.log('refreshed!', this.state.refresh);
            //     }

            //     async componentDidMount() {
            //         const request = await fetch("http://localhost:8090/api/hats/");
            //         const hatData = await request.json();
            //         this.setState({
            //             hats: hatData.hats,
            //         })
            //     }

            //     render() {
            //         return (
            //             <div id="accordion">
            //                 {this.state.hats.map(hat => <AccordionItem key={hat.id} data={hat} refresh={this.refresh} />)}
            //             </div>
            //         )
            //     }
            // }
