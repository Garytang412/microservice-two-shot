import React from 'react'
import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';




export default function UpdateHat() {
    let { id } = useParams();
    // state s:
    const [manufacturer, setManufacturer] = useState("");
    const [model, setModel] = useState("");
    const [color, setColor] = useState("");
    const [url, setUrl] = useState("");
    const [bin, setBin] = useState("");
    const [bins, setBins] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8100/api/bins/")
            .then(res => res.json())
            .then(res => res)
            .then(data => setBins(data.bins));
        fetch(`http://localhost:8080/api/shoes/${id}/`)
            .then(res => res.json())
            .then(res => res)
            .then(data => {
                setManufacturer(data.manufacturer);
                setModel(data.model_name);
                setColor(data.color);
                setUrl(data.pic_url);
            });
    }, [])




    // const handleFabricChange = (e) => {
    //     setFabric(e.target.value)
    // }

    // const handleStyleChange = (e) => {
    //     setStyle(e.target.value);
    // }
    // const handleColorChange = (e) => {
    //     setColor(e.target.value);
    // }
    // const handleUrlChange = (e) => {
    //     setUrl(e.target.value);
    // }
    // const handleLocationChange = (e) => {
    //     setLocation(e.target.value);
    // }

    const handleSubmit = (e) => {
        e.preventDefault();
        const updatedData = {
            manufacturer: manufacturer,
            model_name: model,
            color: color,
            bin: bin,
        }
        console.log(updatedData)
        const updateUrl = `http://localhost:8080/api/shoes/${id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(updatedData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            fetch(updateUrl, fetchConfig)
                .then(resp => resp.json())
                .then(resp => resp)
                .then(data => { console.log(data); window.bin.href = "http://localhost:3000/shoes/" })
        }
        catch (e) {
            console.log("updateFetch error:", e)
        }

    }





    return (
        <div className='container w-50 text-center py-5'>
            <h2>Update A Shoe:</h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="manufacturer">Manufacturer:</label>
                    <input value={manufacturer} onChange={(e) => setManufacturer(e.target.value)} type="text" className="form-control" id='manufacturer' placeholder="manufacturer" aria-label="manufacturer" name="manufacturer" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="model">Model:</label>
                    <input value={model} onChange={(e) => setModel(e.target.value)} type="text" className="form-control" id='model' placeholder="model name" aria-label="model name" name="model_name" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="color">Color:</label>
                    <input value={color} onChange={(e) => setColor(e.target.value)} type="text" className="form-control" id='color' placeholder="color" aria-label="color" name="color" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="pic_url">Picture Url:</label>
                    <input value={url} onChange={(e) => setUrl(e.target.value)} type="url" className="form-control" id='pic_url' placeholder="https://example.com/" aria-label="picture url" name="pic_url" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="inputGroupSelect01">Bin:</label>
                    <select name="bin" onChange={(e) => setBin(e.target.value)} className="form-select" id="inputGroupSelect01">
                        <option value=''>Choose...</option>
                        {bins.map(l => <option value={l.href} key={l.href}>{`${l.closet_name} at size:${l.bin_size},number:${l.bin_number}.`}</option>)}
                    </select>
                </div>
                <button className='btn-lg btn-primary w-25' >Update</button>
            </form>

        </div>

    )
}
