import React, { Component } from 'react'

export default class ShoeForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            bins: [],
            manufacturer: "",
            model_name: "",
            color: "",
            pic_url: "",
            bin: ""
        }
    }
    async componentDidMount() {
        try {
            const resp = await fetch("http://localhost:8100/api/bins/");
            if (resp.ok) {
                const data = await resp.json();
                this.setState({
                    bins: data.bins
                })

            }
            else {
                console.log("location resp error!")
            }
        }
        catch (e) {
            console.log("fetch error:", e)
        }

    }

    handleChange = (e) => {
        const data = e.target.value;
        const name = e.target.name;
        this.setState({
            [name]: data
        });

    }


    handleSubmit = async (e) => {
        e.preventDefault();
        delete this.state.bins;
        const postUrl = "http://localhost:8080/api/shoes/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(this.state),
            headers: { "Content-Type": "application/json" }
        }
        try {
            const resp = await fetch(postUrl, fetchConfig);
            if (resp.ok) {
                this.setState({
                    bins: [],
                    manufacturer: "",
                    model_name: "",
                    color: "",
                    pic_url: "",
                    bin: ""
                })
            }
        }
        catch (e) {
            console.log("post Fetch error", e)
        }

        console.log(this.state)


    }









    render() {
        return (
            <div className='container w-50 text-center py-5'>
                <h2>Add A New Shoe</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="manufacturer">Manufacturer:</label>
                        <input onChange={this.handleChange} type="text" className="form-control" id='manufacturer' placeholder="manufacturer" aria-label="manufacturer" name="manufacturer" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="model">Model:</label>
                        <input onChange={this.handleChange} type="text" className="form-control" id='model' placeholder="model name" aria-label="model name" name="model_name" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="color">Color:</label>
                        <input onChange={this.handleChange} type="text" className="form-control" id='color' placeholder="color" aria-label="color" name="color" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="pic_url">Picture Url:</label>
                        <input onChange={this.handleChange} type="url" className="form-control" id='pic_url' placeholder="https://example.com/" aria-label="picture url" name="pic_url" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="inputGroupSelect01">Bin:</label>
                        <select name="bin" onChange={this.handleChange} className="form-select" id="inputGroupSelect01">
                            <option value=''>Choose...</option>
                            {this.state.bins.map(l => <option value={l.href} key={l.href}>{`${l.closet_name} at number:${l.bin_number},size:${l.bin_size}.`}</option>)}
                        </select>
                    </div>
                    <button className='btn-lg btn-primary w-25' >Add</button>
                </form>

            </div>
        )
    }
}
