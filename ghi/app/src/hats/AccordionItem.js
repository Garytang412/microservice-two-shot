import React from 'react';
import { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

export default function AccordionItem(props) {
    const [show, setShow] = useState("");
    const [fabric, setFabric] = useState("");
    const [style, setStyle] = useState("");
    const [color, setColor] = useState("");
    const [url, setUrl] = useState("");
    const [location, setLocation] = useState("");


    const handleClick = () => {
        if (show === '') setShow("show");
        else setShow("");
    }
    useEffect(() => {
        fetch(`http://localhost:8090/api/hats/${props.data.id}/`)
            .then(res => res.json())
            .then(res => res)
            .then(data => {
                setFabric(data.fabric);
                setStyle(data.style_name);
                setColor(data.color);
                setUrl(data.pic_url);
                setLocation(data.location.name)
            })
    }, [])

    const handleDelete = async () => {
        const deleteUrl = `http://localhost:8090/api/hats/${props.data.id}/`;
        const fetchConfig = {
            method: 'delete',
        };
        const resp = await fetch(deleteUrl, fetchConfig);
        if (resp.ok) {
            props.setRefresh(props.refresh + 1)
            console.log("deleted!")
        }
        else { console.log("delete error!") }

    }

    //console.log(props.refresh)



    return (
        <div className="card rounded my-2 bg-light">
            <div className="card-header" id="headingOne">
                <h5 className="mb-0">
                    <button className="btn btn-outline-primary " onClick={handleClick}>Hat No. {`${props.data.id}`}</button>
                </h5>
            </div>

            <div id="collapseOne" className={`collapse ${show}`} aria-labelledby="headingOne" data-parent="#accordion">
                <div className='container'>
                    <div className="row bg-dark text-light">
                        <div className="col d-flex flex-column justify-content-center">
                            <img src={url} alt="" className="rounded py-2" />
                        </div>
                        <div className="col d-flex flex-column justify-content-center">


                            <h3>Fabric: {fabric}</h3>
                            <h3>Style: {style}</h3>
                            <h3>color: {color}</h3>
                            <h3>Location: {location}</h3>
                            <hr />
                            <button onClick={handleDelete} className='btn btn-info'>Delete</button>
                            <Link to={`/hats/${props.data.id}`} className='btn btn-success my-2'>Update</Link>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}
