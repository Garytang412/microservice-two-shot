from common.json import ModelEncoder
from .models import Shoe, BinVO

#ENCODERS

class BinVODetailEncoder(ModelEncoder):
    model = BinVO
    properties = ["name","import_href","id"]


class ShoeListEncoder(ModelEncoder):
    model = Shoe
    properties = ['manufacturer', 'model_name', 'color', 'id']


class ShoeDetailEncoder(ModelEncoder):
    model = Shoe
    properties = ['manufacturer', 'model_name', 'color',"pic_url", 'id', "bin"]
    encoders = {
        "bin": BinVODetailEncoder(),
    }

