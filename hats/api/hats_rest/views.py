from hats_rest.encoders import HatListEncoder, HatDetailEncoder
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
import json

from hats_rest.models import LocationVO, Hat
# Create your views here.



# handle List GET and POST method and handle List hats for same location id
@require_http_methods(["GET","POST"])
def api_list_hats(request,locaton_id=None):
    # handle GET:
    if request.method == "GET":
        if locaton_id == None:
            hats = {"hats":Hat.objects.all()}
        else:
            hat_list = Hat.objects.filter(location=locaton_id)
            hats = {"hats":hat_list}

        return JsonResponse(hats,encoder=HatListEncoder,safe=False)

    # handle POST:
    else:
        # get all data from request and turn to python object:
        content = json.loads(request.body)

        try: # try if the locationVO in the DB:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            # assign the LocationVO to content["location"]:
            content["location"] = location
        except: #
            return JsonResponse({"message":"no this locatonVO in DB"})

        # create the new hat to DB:
        new_hat = Hat.objects.create(**content)
        # return the new hat detail as json for display.
        return JsonResponse(new_hat,encoder=HatDetailEncoder,safe=False)


@require_http_methods(["DELETE", "GET", "PUT"])
def api_detail_hats(request,pk):
    if request.method == "GET":
        hat = Hat.objects.get(id=pk)
        return JsonResponse(hat,encoder=HatDetailEncoder,safe=False)
    elif request.method == "DELETE":
        count,_ = Hat.objects.filter(id = pk).delete()
        return JsonResponse({"deleted": count >0})

    else:
        # 1. turn request json to python obj:
        content = json.loads(request.body)

        # 2. checking the locationVO:
        try:
            location_href = content['location']
            location = LocationVO.objects.get(import_href=location_href)
            # assign the LocationVO to content["location"]:
            content["location"] = location
        except:
            return JsonResponse({"message":"no this LocationVO in DB"})
        # 3. update the hat:
        Hat.objects.filter(id=pk).update(**content)

        # 4 get the hat with new info
        new_hat = Hat.objects.get(id=pk)
        return JsonResponse(new_hat,encoder=HatDetailEncoder, safe=False)

