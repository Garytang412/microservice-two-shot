import React, { Component } from 'react'

export default class HatForm extends Component {
    constructor(props) {
        super(props);
        this.state = {
            locations: [],
            fabric: "",
            style_name: "",
            color: "",
            pic_url: "",
            location: ""
        }
    }
    async componentDidMount() {
        try {
            const resp = await fetch("http://localhost:8100/api/locations/");
            if (resp.ok) {
                const data = await resp.json();
                this.setState({
                    locations: data.locations
                })

            }
            else {
                console.log("location resp error!")
            }
        }
        catch (e) {
            console.log("fetch error:", e)
        }

    }

    handleChange = (e) => {
        const data = e.target.value;
        const name = e.target.name;
        this.setState({
            [name]: data
        });

    }


    handleSubmit = async (e) => {
        e.preventDefault();
        delete this.state.locations;
        const postUrl = "http://localhost:8090/api/hats/";
        const fetchConfig = {
            method: "post",
            body: JSON.stringify(this.state),
            headers: { "Content-Type": "application/json" }
        }
        try {
            const resp = await fetch(postUrl, fetchConfig);
            if (resp.ok) {
                this.setState({
                    locations: [],
                    fabric: "",
                    style_name: "",
                    color: "",
                    pic_url: "",
                    location: ""
                })
            }
        }
        catch (e) {
            console.log("post Fetch error", e)
        }

        console.log(this.state)


    }









    render() {
        return (
            <div className='container w-50 text-center py-5'>
                <h2>Add A New Hat</h2>
                <form onSubmit={this.handleSubmit}>
                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="fabric">Fabric:</label>
                        <input onChange={this.handleChange} type="text" className="form-control" id='fabric' placeholder="fabric" aria-label="fabric" name="fabric" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="style">Style:</label>
                        <input onChange={this.handleChange} type="text" className="form-control" id='style' placeholder="style name" aria-label="style name" name="style_name" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="color">Color:</label>
                        <input onChange={this.handleChange} type="text" className="form-control" id='color' placeholder="color" aria-label="color" name="color" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="pic_url">Picture Url:</label>
                        <input onChange={this.handleChange} type="url" className="form-control" id='pic_url' placeholder="https://example.com/" aria-label="picture url" name="pic_url" aria-describedby="basic-addon1" />
                    </div>

                    <div className="input-group mb-3">
                        <label className="input-group-text" htmlFor="inputGroupSelect01">Location:</label>
                        <select name="location" onChange={this.handleChange} className="form-select" id="inputGroupSelect01">
                            <option value=''>Choose...</option>
                            {this.state.locations.map(l => <option value={l.href} key={l.href}>{`${l.closet_name} at section:${l.section_number},shelf:${l.shelf_number}.`}</option>)}
                        </select>
                    </div>
                    <button className='btn-lg btn-primary w-25' >Add</button>
                </form>

            </div>
        )
    }
}
