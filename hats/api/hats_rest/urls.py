from django.urls import path
from .views import api_list_hats, api_detail_hats

#localhost:8090/api/
urlpatterns = [
    # POST LIST all Hats:
    path("hats/",api_list_hats),
    
    # Detail Put Delete Hat:
    path("hats/<int:pk>/",api_detail_hats),

    # list out hats in a location
    path("location/<int:locaton_id>/hats/", api_list_hats),
]
