from django.urls import path
from .views import api_list_shoes, api_show_shoe


#localhost:8080/api/
urlpatterns = [
    #POST LIST all shoes:
    path("shoes/", api_list_shoes),
    # Detail Put Delete Hat
    path("shoes/<int:pk>/", api_show_shoe),
    #list out shoes in a location
    path("location/<int:bin_id>/shoes/", api_list_shoes ),
    ]