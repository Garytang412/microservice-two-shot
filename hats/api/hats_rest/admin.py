from django.contrib import admin
from .models import LocationVO, Hat
# Register your models here.

admin.site.register(Hat)
admin.site.register(LocationVO)