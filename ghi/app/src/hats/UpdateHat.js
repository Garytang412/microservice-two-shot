import React from 'react'
import { useParams } from 'react-router-dom';
import { useState, useEffect } from 'react';




export default function UpdateHat() {
    let { id } = useParams();
    // state s:
    const [fabric, setFabric] = useState("");
    const [style, setStyle] = useState("");
    const [color, setColor] = useState("");
    const [url, setUrl] = useState("");
    const [location, setLocation] = useState("");
    const [locations, setLocations] = useState([]);

    useEffect(() => {
        fetch("http://localhost:8100/api/locations/")
            .then(res => res.json())
            .then(res => res)
            .then(data => setLocations(data.locations));
        fetch(`http://localhost:8090/api/hats/${id}/`)
            .then(res => res.json())
            .then(res => res)
            .then(data => {
                setFabric(data.fabric);
                setStyle(data.style_name);
                setColor(data.color);
                setUrl(data.pic_url);
            });
    }, [])




    // const handleFabricChange = (e) => {
    //     setFabric(e.target.value)
    // }

    // const handleStyleChange = (e) => {
    //     setStyle(e.target.value);
    // }
    // const handleColorChange = (e) => {
    //     setColor(e.target.value);
    // }
    // const handleUrlChange = (e) => {
    //     setUrl(e.target.value);
    // }
    // const handleLocationChange = (e) => {
    //     setLocation(e.target.value);
    // }

    const handleSubmit = (e) => {
        e.preventDefault();
        const updatedData = {
            fabric: fabric,
            style_name: style,
            color: color,
            location: location,
        }
        console.log(updatedData)
        const updateUrl = `http://localhost:8090/api/hats/${id}/`;
        const fetchConfig = {
            method: "put",
            body: JSON.stringify(updatedData),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        try {
            fetch(updateUrl, fetchConfig)
                .then(resp => resp.json())
                .then(resp => resp)
                .then(data => { console.log(data); window.location.href = "http://localhost:3000/hats/" })
        }
        catch (e) {
            console.log("updateFetch error:", e)
        }

    }





    return (
        <div className='container w-50 text-center py-5'>
            <h2>Update A Hat:</h2>
            <form onSubmit={handleSubmit}>
                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="fabric">Fabric:</label>
                    <input value={fabric} onChange={(e) => setFabric(e.target.value)} type="text" className="form-control" id='fabric' placeholder="fabric" aria-label="fabric" name="fabric" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="style">Style:</label>
                    <input value={style} onChange={(e) => setStyle(e.target.value)} type="text" className="form-control" id='style' placeholder="style name" aria-label="style name" name="style_name" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="color">Color:</label>
                    <input value={color} onChange={(e) => setColor(e.target.value)} type="text" className="form-control" id='color' placeholder="color" aria-label="color" name="color" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="pic_url">Picture Url:</label>
                    <input value={url} onChange={(e) => setUrl(e.target.value)} type="url" className="form-control" id='pic_url' placeholder="https://example.com/" aria-label="picture url" name="pic_url" aria-describedby="basic-addon1" />
                </div>

                <div className="input-group mb-3">
                    <label className="input-group-text" htmlFor="inputGroupSelect01">Location:</label>
                    <select name="location" onChange={(e) => setLocation(e.target.value)} className="form-select" id="inputGroupSelect01">
                        <option value=''>Choose...</option>
                        {locations.map(l => <option value={l.href} key={l.href}>{`${l.closet_name} at section:${l.section_number},shelf:${l.shelf_number}.`}</option>)}
                    </select>
                </div>
                <button className='btn-lg btn-primary w-25' >Update</button>
            </form>

        </div>

    )
}
